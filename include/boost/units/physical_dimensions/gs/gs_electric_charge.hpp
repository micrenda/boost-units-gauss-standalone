// Boost.Units - A C++ library for zero-overhead dimensional analysis and 
// unit/quantity manipulation and conversion
//
// Copyright (C) 2018 Michele Renda
//
// Distributed under the Boost Software License, Version 1.0. (See
// accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#ifndef BOOST_UNITS_GS_ELECTRIC_CHARGE_DERIVED_DIMENSION_HPP
#define BOOST_UNITS_GS_ELECTRIC_CHARGE_DERIVED_DIMENSION_HPP

#include <boost/units/physical_dimensions/length.hpp>
#include <boost/units/physical_dimensions/mass.hpp>
#include <boost/units/physical_dimensions/time.hpp>

namespace boost {

namespace units {

/// derived dimension for electric charge in GAUSS units :  L^3/2 M^1/2 T^-1
typedef boost::units::make_dimension_list<boost::mpl::list<
		boost::units::dim<boost::units::length_base_dimension,			boost::units::static_rational<3,2>>,
		boost::units::dim<boost::units::mass_base_dimension,      		boost::units::static_rational<1,2>>,
		boost::units::dim<boost::units::time_base_dimension,      		boost::units::static_rational<-1>>
>>::type     gs_electric_charge_dimension;
    
} // namespace units

} // namespace boost

#endif // BOOST_UNITS_GS_ELECTRIC_CHARGE_DERIVED_DIMENSION_HPP
