// Boost.Units - A C++ library for zero-overhead dimensional analysis and 
// unit/quantity manipulation and conversion
//
// Copyright (C) 2018 Michele Renda
//
// Distributed under the Boost Software License, Version 1.0. (See
// accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#ifndef BOOST_UNITS_GAUSS_HPP
#define BOOST_UNITS_GAUSS_HPP

/// \file
/// Includes all the gauss unit headers

#include <string>

#include <boost/units/quantity.hpp>

#include <boost/units/systems/gauss/base.hpp>

#include <boost/units/systems/gauss/dimensionless.hpp>
#include <boost/units/systems/gauss/length.hpp>
#include <boost/units/systems/gauss/mass.hpp>
#include <boost/units/systems/gauss/time.hpp>

#include <boost/units/systems/gauss/acceleration.hpp>
#include <boost/units/systems/gauss/area.hpp>
#include <boost/units/systems/gauss/capacitance.hpp>
#include <boost/units/systems/gauss/conductance.hpp>
#include <boost/units/systems/gauss/current.hpp>
#include <boost/units/systems/gauss/dynamic_viscosity.hpp>
#include <boost/units/systems/gauss/electric_charge.hpp>
#include <boost/units/systems/gauss/electric_potential.hpp>
#include <boost/units/systems/gauss/energy.hpp>
#include <boost/units/systems/gauss/force.hpp>
#include <boost/units/systems/gauss/frequency.hpp>
#include <boost/units/systems/gauss/inductance.hpp>
#include <boost/units/systems/gauss/kinematic_viscosity.hpp>
#include <boost/units/systems/gauss/magnetic_flux_density.hpp>
#include <boost/units/systems/gauss/magnetic_flux.hpp>
#include <boost/units/systems/gauss/mass_density.hpp>
#include <boost/units/systems/gauss/momentum.hpp>
#include <boost/units/systems/gauss/power.hpp>
#include <boost/units/systems/gauss/pressure.hpp>
#include <boost/units/systems/gauss/resistance.hpp>
#include <boost/units/systems/gauss/velocity.hpp>
#include <boost/units/systems/gauss/volume.hpp>
#include <boost/units/systems/gauss/wavenumber.hpp>





#endif // BOOST_UNITS_GAUSS_HPP
