// Boost.Units - A C++ library for zero-overhead dimensional analysis and 
// unit/quantity manipulation and conversion
//
// Copyright (C) 2018 Michele Renda
//
// Distributed under the Boost Software License, Version 1.0. (See
// accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#ifndef BOOST_UNITS_GAUSS_BASE_HPP
#define BOOST_UNITS_GAUSS_BASE_HPP

#include <string>

#include <boost/units/static_constant.hpp>
#include <boost/units/unit.hpp>
#include <boost/units/make_system.hpp>

#include <boost/units/base_units/cgs/centimeter.hpp>
#include <boost/units/base_units/cgs/gram.hpp>
#include <boost/units/base_units/si/second.hpp>

namespace boost {

namespace units { 

namespace gauss {

/// placeholder class defining cgs unit system
typedef make_system<boost::units::cgs::centimeter_base_unit, 
                    boost::units::cgs::gram_base_unit, 
                    boost::units::si::second_base_unit>::type system;

/// various unit typedefs for convenience
typedef unit<dimensionless_type,system>         dimensionless;

} // namespace gauss
                                                    
} // namespace units

} // namespace boost

#endif // BOOST_UNITS_GAUSS_BASE_HPP
