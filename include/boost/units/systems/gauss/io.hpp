// Boost.Units - A C++ library for zero-overhead dimensional analysis and 
// unit/quantity manipulation and conversion
//
// Copyright (C) 2018 Michele Renda
//
// Distributed under the Boost Software License, Version 1.0. (See
// accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#ifndef BOOST_UNITS_GAUSS_IO_HPP
#define BOOST_UNITS_GAUSS_IO_HPP

#include <boost/units/io.hpp>
#include <boost/units/reduce_unit.hpp>
#include <boost/units/systems/gauss.hpp>

namespace boost {

namespace units { 

inline std::string name_string(const reduce_unit<gauss::acceleration>::type&) { return "galileo"; }
inline std::string symbol_string(const reduce_unit<gauss::acceleration>::type&) { return "Gal"; }

inline std::string name_string(const reduce_unit<gauss::dynamic_viscosity>::type&) { return "poise"; }
inline std::string symbol_string(const reduce_unit<gauss::dynamic_viscosity>::type&) { return "P"; }

inline std::string name_string(const reduce_unit<gauss::energy>::type&) { return "erg"; }
inline std::string symbol_string(const reduce_unit<gauss::energy>::type&) { return "erg"; }

inline std::string name_string(const reduce_unit<gauss::force>::type&) { return "dyne"; }
inline std::string symbol_string(const reduce_unit<gauss::force>::type&) { return "dyn"; }

inline std::string name_string(const reduce_unit<gauss::kinematic_viscosity>::type&) { return "stoke"; }
inline std::string symbol_string(const reduce_unit<gauss::kinematic_viscosity>::type&) { return "St"; }

inline std::string name_string(const reduce_unit<gauss::pressure>::type&) { return "barye"; }
inline std::string symbol_string(const reduce_unit<gauss::pressure>::type&) { return "Ba"; }

inline std::string name_string(const reduce_unit<gauss::wavenumber>::type&) { return "kayser"; }
inline std::string symbol_string(const reduce_unit<gauss::wavenumber>::type&) { return "K"; }

inline std::string name_string(const reduce_unit<gauss::current>::type&)   { return "statampere"; }
inline std::string symbol_string(const reduce_unit<gauss::current>::type&) { return "statA"; }

inline std::string name_string(const reduce_unit<gauss::electric_charge>::type&)   { return "statcoulomb"; }
inline std::string symbol_string(const reduce_unit<gauss::electric_charge>::type&) { return "statC"; }

inline std::string name_string(const reduce_unit<gauss::electric_potential>::type&)   { return "statvolt"; }
inline std::string symbol_string(const reduce_unit<gauss::electric_potential>::type&) { return "statV"; }

inline std::string name_string(const reduce_unit<gauss::resistance>::type&)   { return "statohm"; }
inline std::string symbol_string(const reduce_unit<gauss::resistance>::type&) { return "statOhm"; }

inline std::string name_string(const reduce_unit<gauss::conductance>::type&)   { return "statmho"; }
inline std::string symbol_string(const reduce_unit<gauss::conductance>::type&) { return "statMho"; }

inline std::string name_string(const reduce_unit<gauss::capacitance>::type&)   { return "statfarad"; }
inline std::string symbol_string(const reduce_unit<gauss::capacitance>::type&) { return "statF"; }

inline std::string name_string(const reduce_unit<gauss::inductance>::type&)   { return "stathenry"; }
inline std::string symbol_string(const reduce_unit<gauss::inductance>::type&) { return "statH"; }

inline std::string name_string(const reduce_unit<gauss::magnetic_flux_density>::type&)   { return "stattesla"; }
inline std::string symbol_string(const reduce_unit<gauss::magnetic_flux_density>::type&) { return "statT"; }

inline std::string name_string(const reduce_unit<gauss::magnetic_flux>::type&)   { return "statweber"; }
inline std::string symbol_string(const reduce_unit<gauss::magnetic_flux>::type&) { return "statWb"; }


} // namespace units

} // namespace boost

#endif // BOOST_UNITS_GAUSS_IO_HPP
