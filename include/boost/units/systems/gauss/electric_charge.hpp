// Boost.Units - A C++ library for zero-overhead dimensional analysis and 
// unit/quantity manipulation and conversion
//
// Copyright (C) 2018 Michele Renda
//
// Distributed under the Boost Software License, Version 1.0. (See
// accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#ifndef BOOST_UNITS_GAUSS_ELECTRIC_CHARGE_HPP
#define BOOST_UNITS_GAUSS_ELECTRIC_CHARGE_HPP

#include <boost/units/systems/gauss/base.hpp>
#include <boost/units/physical_dimensions/gs/gs_electric_charge.hpp>

namespace boost {

namespace units { 

namespace gauss {


typedef unit<gs_electric_charge_dimension,gauss::system>   electric_charge;

BOOST_UNITS_STATIC_CONSTANT(statcoulomb,electric_charge);
BOOST_UNITS_STATIC_CONSTANT(statcoulombs,electric_charge);

} // namespace gauss

} // namespace units

} // namespace boost

#endif // BOOST_UNITS_GAUSS_ELECTRIC_CHARGE_HPP
