// Boost.Units - A C++ library for zero-overhead dimensional analysis and 
// unit/quantity manipulation and conversion
//
// Copyright (C) 2018 Michele Renda
//
// Distributed under the Boost Software License, Version 1.0. (See
// accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#ifndef BOOST_UNITS_GAUSS_ENERGY_HPP
#define BOOST_UNITS_GAUSS_ENERGY_HPP

#include <boost/units/systems/gauss/base.hpp>
#include <boost/units/physical_dimensions/energy.hpp>

namespace boost {

namespace units { 

namespace gauss {

typedef unit<energy_dimension,gauss::system>   energy;
    
BOOST_UNITS_STATIC_CONSTANT(erg,energy);
BOOST_UNITS_STATIC_CONSTANT(ergs,energy);

} // namespace gauss

} // namespace units

} // namespace boost

#endif // BOOST_UNITS_GAUSS_ENERGY_HPP
